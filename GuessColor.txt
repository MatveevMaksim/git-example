﻿import java.util.Scanner;

//Необходимо отгадать цвет
 
public class C04_GuessColor {

    public static void main(String[] args) {
        System.out.println("Угадайте задуманный цвет с шести попыток");
        System.out.println("Для выхода из программы введите - exit");

        final int MAX_ATTEMPT = 6;
        int attempt = 0;
//        String color = "red";
	  String color = "blue";
        Scanner in = new Scanner(System.in);

        while (attempt < MAX_ATTEMPT) {
            attempt++;
            System.out.println("Попытка " + attempt + ":");
            String value = in.next();

            if (value.equals("exit")) {
                break;
            }

            if (!value.equals(color)) {
                System.out.println("Вы не угадали. Количество оставшихся попыток: " + (MAX_ATTEMPT-attempt) + " ");
                if (attempt <= 5) {
                    System.out.println("Попробуйте ещё раз.");
                }

                continue;
            }

            System.out.println("Поздравлям, Вы угадали с " + attempt + " попытки");
            break;
        }

        System.out.println("Игра окончена!");
    }
}